//
//  RCTMobAd.h
//  ReactNativeWelcomeApp
//
//  Created by Marwan Toutounji on 3/16/21.
//

#import <React/RCTBridgeModule.h>

@interface MobAdiOS : NSObject<RCTBridgeModule>

@end
