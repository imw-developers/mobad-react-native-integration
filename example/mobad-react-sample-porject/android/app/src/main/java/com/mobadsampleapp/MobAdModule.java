package com.mobadsampleapp;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableMapKeySetIterator;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableNativeMap;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imagineworks.mobad_sdk.MobAd;
import com.imagineworks.mobad_sdk.models.Category;
import com.imagineworks.mobad_sdk.models.Language;
import com.imagineworks.mobad_sdk.models.Subcategory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class MobAdModule extends ReactContextBaseJavaModule {
    private final String FIELD_LIMIT_EXCEEDED = "limitExceeded";
    private final String FIELD_NEW_MAX_ADS_PER_DAY_VALUE = "newMaxAdsPerDayValue";

    @NonNull
    @Override
    public String getName() {
        return "MobAdAndroid";
    }

    @Nullable
    private ReactApplicationContext context = null;

    private MobAd mobAd;

    public MobAdModule(ReactApplicationContext context) {
        super(context);
        this.context = context;
        this.mobAd = new MobAd(context);
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public boolean checkUserEnabledNotificationListenerSettings() {
        return mobAd.checkUserEnabledNotificationListenerSettings();
    }

    @ReactMethod
    public void goToNotificationListenerSettings() {
        if (context == null) {
            return;
        }
        mobAd.goToNotificationListenerSettings(context.getCurrentActivity());
    }

    @ReactMethod
    public void startMobAdService(boolean requireReadPhoneStatePermissionIfNotGranted, String rewardsAppPackageName) {
        if (context == null) {
            return;
        }
        mobAd.startMobAdService(context.getCurrentActivity(), requireReadPhoneStatePermissionIfNotGranted, rewardsAppPackageName);
    }

    @ReactMethod
    public void checkMobAdPermissionsAndStartService(String rewardsAppPackageName) {
        if (context == null) {
            return;
        }
        mobAd.checkMobAdPermissionsAndStartService(context.getCurrentActivity(), rewardsAppPackageName);
    }

    @ReactMethod
    public void requestInterstitialAd(int delayMillis) {
        mobAd.requestInterstitialAd(delayMillis);
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    boolean hasReadPhoneStatePermission() {
        return mobAd.hasReadPhoneStatePermission();
    }

    @ReactMethod
    void requestReadPhoneStatePermission(Promise permissionResultPromise) {
        if (context == null) {
            return;
        }
        mobAd.requestReadPhoneStatePermission(context.getCurrentActivity(), new MobAd.PermissionActionListener() {
            @Override
            public void onPermissionGranted() {
                PromiseHelper.resolveWithBoolValue(permissionResultPromise, true);
            }

            @Override
            public void onPermissionDenied() {
                PromiseHelper.resolveWithBoolValue(permissionResultPromise, false);
            }
        });
    }

    @ReactMethod
    void requestDrawOverAppsPermission() {
        if (context == null) {
            return;
        }
        mobAd.requestDrawOverAppsPermission(context.getCurrentActivity());
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    boolean canDrawOverOtherApps() {
        return mobAd.canDrawOverlays();
    }

    @ReactMethod
    void setMaximumAdsPerDay(int maxAdsPerDay, Promise completionPromise) {
        mobAd.setMaximumAdsPerDay(maxAdsPerDay, new MobAd.SetMaxAdsPerDayCompletionListener() {
            @Override
            public void onSuccess(boolean b, int i) {
                WritableMap map = new WritableNativeMap();

                map.putBoolean(FIELD_LIMIT_EXCEEDED, b);
                map.putInt(FIELD_NEW_MAX_ADS_PER_DAY_VALUE, i);
                PromiseHelper.resolveWithValue(completionPromise, map);
            }

            @Override
            public void onError(int i, String s) {
                PromiseHelper.returnListenerOnError(completionPromise, i, s);
            }
        });
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    int getMaximumAdsPerDay() {
        return mobAd.getMaximumAdsPerDay();
    }

    @ReactMethod
    void setAdsEnabled(boolean adsEnabled) {
        mobAd.setAdsEnabled(adsEnabled);
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    boolean getAdsEnabled() {
        return mobAd.getAdsEnabled();
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    String getUserEmail() {
        return mobAd.getUserEmail();
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public boolean isUserInitialized() {
        return mobAd.isUserInitialized();
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public boolean isUserFailedInit() {
        return mobAd.isUserFailedInit();
    }

    //region INTERESTS
    //=================
    @ReactMethod
    public void getCategories(Promise resultPromise) {
        mobAd.getCategories(new MobAd.GetCategoriesCompletionListener() {
            @Override
            public void onSuccess(@NonNull List<Category> list) {
                try {
                    PromiseHelper.resolveWithValue(resultPromise, ModelHelper.getWritableNativeArrayFromList(list));
                } catch (JSONException e) {
                    PromiseHelper.returnJsonError(resultPromise);
                }
            }

            @Override
            public void onError(int i, String s) {
                PromiseHelper.returnListenerOnError(resultPromise, i, s);
            }
        });
    }

    @ReactMethod
    public void updateInterests(ReadableArray listOfInterests, Promise completionPromise) {
        try {
            List<Subcategory> subcategoryList = ModelHelper.getListFromReadableArray(listOfInterests, Subcategory.class);
            mobAd.updateInterests(subcategoryList, new MobAd.UpdateInterestsCompletionListener() {
                @Override
                public void onSuccess() {
                    PromiseHelper.returnSuccessWithNoResult(completionPromise);
                }

                @Override
                public void onError(int i, String s) {
                    PromiseHelper.returnListenerOnError(completionPromise, i ,s);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            PromiseHelper.returnError(completionPromise, e.getClass().toString(), e.getMessage());
        }
    }
    //endregion

    //region USERS
    @ReactMethod
    public void initializeUser(String email, String password, String languageCode, Promise completionPromise) {
        mobAd.initializeUser(email, password, languageCode, new MobAd.InitializeCompletionListener() {
            @Override
            public void onSuccess() {
                PromiseHelper.returnSuccessWithNoResult(completionPromise);
            }

            @Override
            public void onError(int i, String s) {
                PromiseHelper.returnListenerOnError(completionPromise, i, s);
            }
        });
    }

    @ReactMethod
    public void cancelInitUser() {
        mobAd.cancelInitUser();
    }
    //endregion

    //region LANGUAGES
    @ReactMethod
    public void getAdLanguages(Promise resultPromise) {
        mobAd.getAdLanguages(new MobAd.GetAdLanguagesCompletionListener() {
            @Override
            public void onSuccess(List<Language> list) {
                try {
                    PromiseHelper.resolveWithValue(resultPromise, ModelHelper.getWritableNativeArrayFromList(list));
                } catch (JSONException e) {
                    PromiseHelper.returnJsonError(resultPromise);
                }
            }

            @Override
            public void onError(int i, String s) {
                PromiseHelper.returnListenerOnError(resultPromise, i, s);
            }
        });
    }

    @ReactMethod
    public void updateAdLanguages(ReadableArray listOfLanguages, Promise completionPromise) {
        try {
            List<Language> languagesList = ModelHelper.getListFromReadableArray(listOfLanguages, Language.class);
            mobAd.updateAdLanguages(languagesList, new MobAd.UpdateAdLanguagesCompletionListener() {
                @Override
                public void onSuccess() {
                    PromiseHelper.returnSuccessWithNoResult(completionPromise);
                }

                @Override
                public void onError(int i, String s) {
                    PromiseHelper.returnListenerOnError(completionPromise, i, s);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            PromiseHelper.returnError(completionPromise, e.getClass().toString(), e.getMessage());
        }
    }
    //endregion

    private static class PromiseHelper {
        private static final String ERROR_CODE_JSON_PARSE_ERROR = "JSON_PARSE_ERROR";
        private static final String ERROR_MESSAGE_ERROR_PARSING_JSON = "Error parsing JSON!";

        public static void returnSuccessWithNoResult(Promise promise) {
            promise.resolve(null);
        }

        public static void returnListenerOnError(Promise promise, int code, String message) {
            promise.reject(code + "", new Exception(message));
        }

        public static void returnError(Promise promise, String code, String message) {
            promise.reject(code, new Exception(message));
        }

        public static void returnJsonError(Promise promise) {
            promise.reject(ERROR_CODE_JSON_PARSE_ERROR, new Exception(ERROR_MESSAGE_ERROR_PARSING_JSON));
        }

        public static <T> void resolveWithValue(Promise promise, T value) {
            promise.resolve(value);
        }

        public static void resolveWithBoolValue(Promise promise, boolean value) {
            promise.resolve(value);
        }
    }

    private static class ModelHelper {
        static <T> WritableNativeArray getWritableNativeArrayFromList(List<T> list) throws JSONException {
            Gson gsonInstance = new Gson();
            WritableNativeArray array = new WritableNativeArray();

            for (int i = 0; i < list.size(); i++) {
                T currentItem = list.get(i);
                JSONObject currentItemAsJSONObject = new JSONObject(gsonInstance.toJson(currentItem));
                array.pushMap(JSONHelper.convertJsonToMap(currentItemAsJSONObject));
            }

            return array;
        }

        static <T> List<T> getListFromReadableArray(ReadableArray array, Class<T> type) throws JSONException {
            Gson gsonInstance = new Gson();
            JSONArray jsonArray = JSONHelper.convertArrayToJson(array);
            Type listItemType = TypeToken.getParameterized(List.class, type).getType();
            return gsonInstance.fromJson(jsonArray.toString(), listItemType);
        }
    }

    private static class JSONHelper {
        static WritableMap convertJsonToMap(JSONObject jsonObject) throws JSONException {
            WritableMap map = new WritableNativeMap();

            Iterator<String> iterator = jsonObject.keys();
            while (iterator.hasNext()) {
                String key = iterator.next();
                Object value = jsonObject.get(key);
                if (value instanceof JSONObject) {
                    map.putMap(key, convertJsonToMap((JSONObject) value));
                } else if (value instanceof  JSONArray) {
                    map.putArray(key, convertJsonToArray((JSONArray) value));
                } else if (value instanceof  Boolean) {
                    map.putBoolean(key, (Boolean) value);
                } else if (value instanceof  Integer) {
                    map.putInt(key, (Integer) value);
                } else if (value instanceof  Double) {
                    map.putDouble(key, (Double) value);
                } else if (value instanceof String)  {
                    map.putString(key, (String) value);
                } else {
                    map.putString(key, value.toString());
                }
            }
            return map;
        }

        private static WritableArray convertJsonToArray(JSONArray jsonArray) throws JSONException {
            WritableArray array = new WritableNativeArray();

            for (int i = 0; i < jsonArray.length(); i++) {
                Object value = jsonArray.get(i);
                if (value instanceof JSONObject) {
                    array.pushMap(convertJsonToMap((JSONObject) value));
                } else if (value instanceof  JSONArray) {
                    array.pushArray(convertJsonToArray((JSONArray) value));
                } else if (value instanceof  Boolean) {
                    array.pushBoolean((Boolean) value);
                } else if (value instanceof  Integer) {
                    array.pushInt((Integer) value);
                } else if (value instanceof  Double) {
                    array.pushDouble((Double) value);
                } else if (value instanceof String)  {
                    array.pushString((String) value);
                } else {
                    array.pushString(value.toString());
                }
            }
            return array;
        }

        static JSONObject convertMapToJson(ReadableMap readableMap) throws JSONException {
            JSONObject object = new JSONObject();
            ReadableMapKeySetIterator iterator = readableMap.keySetIterator();
            while (iterator.hasNextKey()) {
                String key = iterator.nextKey();
                switch (readableMap.getType(key)) {
                    case Null:
                        object.put(key, JSONObject.NULL);
                        break;
                    case Boolean:
                        object.put(key, readableMap.getBoolean(key));
                        break;
                    case Number:
                        object.put(key, readableMap.getDouble(key));
                        break;
                    case String:
                        object.put(key, readableMap.getString(key));
                        break;
                    case Map:
                        object.put(key, convertMapToJson(readableMap.getMap(key)));
                        break;
                    case Array:
                        object.put(key, convertArrayToJson(readableMap.getArray(key)));
                        break;
                }
            }
            return object;
        }

        static JSONArray convertArrayToJson(ReadableArray readableArray) throws JSONException {
            JSONArray array = new JSONArray();
            for (int i = 0; i < readableArray.size(); i++) {
                switch (readableArray.getType(i)) {
                    case Null:
                        break;
                    case Boolean:
                        array.put(readableArray.getBoolean(i));
                        break;
                    case Number:
                        array.put(readableArray.getDouble(i));
                        break;
                    case String:
                        array.put(readableArray.getString(i));
                        break;
                    case Map:
                        array.put(convertMapToJson(readableArray.getMap(i)));
                        break;
                    case Array:
                        array.put(convertArrayToJson(readableArray.getArray(i)));
                        break;
                }
            }
            return array;
        }
    }
}
