// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.4 (swiftlang-1205.0.26.9 clang-1205.0.19.55)
// swift-module-flags: -target armv7-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name MobAdSDK
import AVKit
import AdSupport
import CallKit
import CommonCrypto
import CoreLocation
import CoreTelephony
import Foundation
@_exported import MobAdSDK
import NotificationCenter
import Swift
import UIKit
import UserNotifications
import UserNotificationsUI
public struct CarouselItem : Swift.Codable {
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public enum MediaType : Swift.Codable {
  case carousel(_: [MobAdSDK.CarouselItem]?)
  case image
  case text
  case video
}
extension MediaType {
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
@objc public enum MediaTypeCaste : Swift.Int {
  case carousel
  case image
  case text
  case video
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public enum NotificationCategory {
  case generic
  public func instance(with actions: [UserNotifications.UNNotificationAction]) -> UserNotifications.UNNotificationCategory
  public static func == (a: MobAdSDK.NotificationCategory, b: MobAdSDK.NotificationCategory) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@_hasMissingDesignatedInitializers public class CachManager {
  @objc deinit
}
@objc public enum MobAdError : Swift.Int {
  case accessTokenMissing
  case accountAlreadyExists
  case appIdentifierMissing
  case deviceIdentifierMissing
  case emptyData
  case invalidLanguage
  case nounceMissing
  case missingAdServiceStatus
  case missingMobileConfig
  case missingCoin
  case missingToken
  case missingLanguages
  case missingUser
  case sdkIdentifierMissing
  case userNotSignedIn
  case undetermined
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@_hasMissingDesignatedInitializers @objc(MASubcategory) public class Subcategory : ObjectiveC.NSObject, Swift.Codable {
  @objc public var name: Swift.String? {
    get
  }
  public var _id: Swift.Int? {
    get
  }
  @objc public var id: Foundation.NSNumber? {
    @objc get
    @objc set
  }
  public var _categoryId: Swift.Int? {
    get
  }
  @objc public var categoryId: Foundation.NSNumber? {
    @objc get
    @objc set
  }
  @objc override dynamic public init()
  @objc deinit
  public func encode(to encoder: Swift.Encoder) throws
  required public init(from decoder: Swift.Decoder) throws
}
@_hasMissingDesignatedInitializers @objc public class Interests : ObjectiveC.NSObject, Swift.Codable {
  @objc public func allCategories() -> [MobAdSDK.Category]
  @objc public func allSubcategories() -> [MobAdSDK.Subcategory]
  @objc public func category(forIndex index: Swift.Int) -> MobAdSDK.Category
  @objc public func subcategories(forCategoryId id: Swift.Int) -> [MobAdSDK.Subcategory]
  @objc public func subcategories(for category: MobAdSDK.Category) -> [MobAdSDK.Subcategory]
  @objc public func subcategories(forCategoryIndex index: Swift.Int) -> [MobAdSDK.Subcategory]
  @objc public func subcategory(for indexPath: Foundation.IndexPath) -> MobAdSDK.Subcategory
  @objc override dynamic public init()
  @objc deinit
  public func encode(to encoder: Swift.Encoder) throws
  required public init(from decoder: Swift.Decoder) throws
}
@_hasMissingDesignatedInitializers @objc public class Language : ObjectiveC.NSObject {
  @objc public var code: Swift.String? {
    get
  }
  @objc public var name: Swift.String? {
    get
  }
  @objc public var nativeName: Swift.String? {
    get
  }
  @objc override dynamic public init()
  @objc deinit
}
public typealias Place = CoreLocation.CLPlacemark
@_hasMissingDesignatedInitializers @objc(MACategory) public class Category : ObjectiveC.NSObject, Swift.Codable {
  @objc public var name: Swift.String? {
    get
  }
  public var _id: Swift.Int? {
    get
  }
  @objc public var id: Foundation.NSNumber? {
    @objc get
    @objc set
  }
  @objc override dynamic public init()
  @objc deinit
  public func encode(to encoder: Swift.Encoder) throws
  required public init(from decoder: Swift.Decoder) throws
}
public typealias SyncResult = UIKit.UIBackgroundFetchResult
public enum Event : Swift.Int {
  case call
  case locationChange
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc public class MobAdSDK : ObjectiveC.NSObject {
  @objc public var backgroundSessionCompletionHandler: (() -> Swift.Void)?
  @objc public static let shared: MobAdSDK.MobAdSDK
  @objc deinit
}
@objc extension MobAdSDK {
  @objc dynamic public var adServiceActive: Swift.Bool {
    @objc get
  }
  @objc dynamic public var isSignedIn: Swift.Bool {
    @objc get
  }
  @objc dynamic public var user: MobAdSDK.User? {
    @objc get
  }
  @objc dynamic public var userMaximumAdsPerDay: Swift.Int {
    @objc get
  }
  @objc dynamic public var languageCode: Swift.String {
    @objc get
  }
  @objc dynamic public var preferredAdLanguages: [Swift.String] {
    @objc get
  }
}
@objc extension MobAdSDK {
  @objc dynamic public func handle(_ response: UserNotifications.UNNotificationResponse) -> Swift.Bool
  @objc dynamic public func handleRemoteNotification(_ payload: [Swift.AnyHashable : Any], completion: @escaping (MobAdSDK.SyncResult) -> Swift.Void) -> Swift.Bool
  @objc dynamic public func handleForegroundNotification(_ notification: UserNotifications.UNNotification, completion: @escaping (UserNotifications.UNNotificationPresentationOptions) -> Swift.Void) -> Swift.Bool
  @objc dynamic public func initializeWith(identifier: Swift.String, group: Swift.String)
  @nonobjc public func activate(for events: [MobAdSDK.Event]? = nil)
  @objc(activate:) dynamic public func objc_activate(for eventsValues: [Swift.Int]? = nil)
  @objc dynamic public func requestAlwaysAuthorizationForLocationMonitoring()
  @objc dynamic public func canAskPermissionForAlwaysMonitoringLocation() -> Swift.Bool
  @nonobjc public func scheduleAdNotification(after time: Foundation.TimeInterval = 10, mediaType: MobAdSDK.MediaType = .text)
  @objc dynamic public func scheduleAdNotification(after time: Foundation.TimeInterval = 10, mediaType: MobAdSDK.MediaTypeCaste = .text)
  @objc dynamic public func signOutUser()
  @objc dynamic public func syncAdsInBackground(completion: ((MobAdSDK.SyncResult) -> Swift.Void)?)
  @objc dynamic public func requestInAppInvasiveAd(delay: Foundation.TimeInterval)
}
@objc extension MobAdSDK {
  @objc dynamic public func adService(activate: Swift.Bool, completion: @escaping (Swift.Bool, MobAdSDK.MobAdError) -> Swift.Void)
  @objc dynamic public func allInterests(completion: @escaping (MobAdSDK.Interests?, MobAdSDK.MobAdError) -> Swift.Void)
  @objc dynamic public func getSupportedLanguages(completion: @escaping (Swift.Bool, [MobAdSDK.Language]?, MobAdSDK.MobAdError) -> Swift.Void)
  @objc dynamic public func getUserInterests(completion: ((Swift.Bool, [Swift.Int]?, MobAdSDK.MobAdError) -> Swift.Void)?)
  @objc dynamic public func getUserProfile(completion: ((Swift.Bool, MobAdSDK.User?, MobAdSDK.MobAdError) -> Swift.Void)?)
  @objc dynamic public func initiateUser(email: Swift.String?, password: Swift.String?, countryCode: Swift.String?, languageCode: Swift.String?, completion: ((Swift.Bool, MobAdSDK.MobAdError) -> Swift.Void)?)
  @objc dynamic public func registerRemoteNotifications(token: Swift.String, completion: ((Swift.Bool) -> Swift.Void)?)
  @objc dynamic public func setUserInterests(subcategoriesIds: [Swift.Int], completion: ((Swift.Bool, MobAdSDK.MobAdError) -> Swift.Void)?)
  @objc dynamic public func updateUserProfile(countryCode: Swift.String?, languageCode: Swift.String?, maxAdsPerDay: Foundation.NSNumber?, preferredAdLanguagesCodes: [Swift.String]?, completion: ((Swift.Bool, Foundation.NSNumber?, MobAdSDK.MobAdError) -> Swift.Void)?)
}
@objc extension MobAdSDK {
  @objc public static func mobAdErrorName(_ enumValue: MobAdSDK.MobAdError) -> Swift.String
}
@_inheritsConvenienceInitializers @objc open class MobAdNotificationViewController : UIKit.UIViewController, UserNotificationsUI.UNNotificationContentExtension {
  @objc override dynamic open func viewDidLoad()
  @objc open func didReceive(_ notification: UserNotifications.UNNotification)
  @objc override dynamic open func viewDidLayoutSubviews()
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
public typealias SyncViewModelRecordRowInfo = (name: Swift.String, url: Foundation.URL, expiryDate: Foundation.Date, image: UIKit.UIImage?, mediaData: Foundation.Data?)
public typealias SyncViewModelScheduledAdRowInfo = (title: Swift.String, mediaType: Swift.String, thumbnail: Foundation.URL?, date: Foundation.Date?)
public class SyncViewModel {
  public init()
  public var records: MobAdSDK.SyncViewModel.Records {
    get
  }
  public var scheduledAds: MobAdSDK.SyncViewModel.ScheduledAds {
    get
  }
  public var recentSyncDate: Foundation.Date? {
    get
  }
  public var adsForSchedulingDate: Foundation.Date? {
    get
  }
  public var scheduledAdsDate: Foundation.Date? {
    get
  }
  @objc deinit
}
extension SyncViewModel {
  @_hasMissingDesignatedInitializers public class Records {
    public var numberOfRows: Swift.Int {
      get
    }
    public func infoForRow(at index: Swift.Int) -> MobAdSDK.SyncViewModelRecordRowInfo
    @objc deinit
  }
}
extension SyncViewModel {
  @_hasMissingDesignatedInitializers public class ScheduledAds {
    public var numberOfRows: Swift.Int {
      get
    }
    public func infoForRow(at index: Swift.Int) -> MobAdSDK.SyncViewModelScheduledAdRowInfo
    @objc deinit
  }
}
@_hasMissingDesignatedInitializers @objc public class User : ObjectiveC.NSObject, Swift.Codable {
  @objc public var countryCode: Swift.String? {
    get
  }
  @objc public var email: Swift.String? {
    get
  }
  @objc override dynamic public init()
  @objc deinit
  public func encode(to encoder: Swift.Encoder) throws
  required public init(from decoder: Swift.Decoder) throws
}
extension MobAdSDK.MediaTypeCaste : Swift.Equatable {}
extension MobAdSDK.MediaTypeCaste : Swift.Hashable {}
extension MobAdSDK.MediaTypeCaste : Swift.RawRepresentable {}
extension MobAdSDK.NotificationCategory : Swift.Equatable {}
extension MobAdSDK.NotificationCategory : Swift.Hashable {}
extension MobAdSDK.MobAdError : Swift.Equatable {}
extension MobAdSDK.MobAdError : Swift.Hashable {}
extension MobAdSDK.MobAdError : Swift.RawRepresentable {}
extension MobAdSDK.Event : Swift.Equatable {}
extension MobAdSDK.Event : Swift.Hashable {}
extension MobAdSDK.Event : Swift.RawRepresentable {}
