/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {
  Alert,
  Button,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  NativeModules,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

import MobAd from './MobAdSDK';

const App: () => React$Node = () => {
  var isMobadInitialized = false;

  var mobad = new MobAd();
  var [serviceEnabled, setServiceEnabled] = useState(mobad.isAdServiceEnabled())
  var [allAdLanguages, setAllAdLanguages] = useState('Loading')
  var [selectedAdLanguages, setSelectedAdLanguages] = useState('Loading')

  initializeMobAdSDK()
  initializeMobAdUserIfNeeded()
  // initializeMobAdUser()
  initalizeAdLanguages()

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Welcome to Mobad React Sample Client App</Text>
              <Text style={styles.sectionDescription}>
                This is a test app created to integrate MobAd with React Native
              </Text>
              <Text style={styles.mobadStatusContainer}>
                Ad service status: <Text style={styles.mobadHighlight}>{serviceEnabled?"Enabled":"Disabled"}</Text>
              </Text>
              <Text style={styles.mobadStatusContainer}>
                All Languages: <Text style={styles.mobadHighlight}>{allAdLanguages}</Text>
              </Text>
              <Text style={styles.mobadStatusContainer}>
                All Selected Languages: <Text style={styles.mobadHighlight}>{selectedAdLanguages}</Text>
              </Text>
              <Button
                title="(De)Activate service?"
                onPress={onTapActivateAdService}
              />
              <Button
                title="Get Categories"
                onPress={onTapGetCategories}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );

  //#region WRAPPERS
  async function activateService(activate) {
    const isCurrentlyActive = mobad.isAdServiceEnabled()

    const alertTitle = 'Ad Service'
    if (isCurrentlyActive === activate) {
      Alert.alert(alertTitle, 
        `Service is already ${isCurrentlyActive?"ENABLED":"DISABLED"}`)
    } else {
      try {
        await mobad.activateAdService(activate)
        setServiceEnabled(activate)
      } catch(e) {
        Alert.alert(alertTitle, 
          `Something went wrong while ${activate?"ENABLING":"DISABLING"} your service, error: ${e}`)
      }
    }
  }

  function initializeMobAdSDK() {
    mobad.ios.initialize('m0b@ds@mpl3@pp1d', 'group.com.imagineworks.mobad.clientapp')
  }

  function initializeMobAdUser() {
    const email = "mcghorayeb@gmail.com"
    const alertTitle = 'User'
    mobad.initializeUser(email, null, null).then(() => {
      Alert.alert(alertTitle, "Initialize user successfully");
      mobad.activate()
    }, (error) => {
      Alert.alert(alertTitle, "Fail to initalize user, error: " + error);
    })
  }

  function initalizeAdLanguages() {
    mobad.getLanguages().then((languages) => {
      setAllAdLanguages(languagesNamesStringFromArray(languages))
      setSelectedAdLanguages(languagesNamesStringFromArray(languages.filter((language) => { return language.isSelected }))) 
    }, (error) => {
      Alert.alert('Language', 'Fail to load languages, error: ' + error)
    })
  }

  function initializeMobAdUserIfNeeded() {
    if (!mobad.isSignedIn()) {
      initializeMobAdUser()
    } else {
      mobad.activate()
    }
  }

  function getCategories() {
    const alertTitle = 'Categories'
    mobad.getCategories().then((categories) => {
      printCategories(categories)
      Alert.alert(alertTitle, "Categories are retrieved")
    }, (error) => {
      Alert.alert(alertTitle, "Fail to retrieve categories, error" + error)
    })
  }

  function updateInterests() {
    mobad.getCategories().then((categories) => {
      const subcategoriesIDs = categories.flatMap((category) => {
        return category.subcategories.map((subcategory) => {
          return subcategory.id
        })
      }).filter((id) => {
        return (id != 5) && (id != 6)
      })

      const alertTitle = "Interests"
      mobad.updateInterests(subcategoriesIDs).then((result) => {
        console.log(`=> Result ${result}`)
        Alert.alert(alertTitle, "Interests Updated")
      }, (error) => {
        Alert.alert(alertTitle, "Fail to update interests, error: " + error)
      })
    }, (error) => {
      console.log("=> Couldn't get interests")
    })
  }

  function updateSelectedAdLanguages() {
    mobad.updateAdLanguages(['en', 'ar']).then(() => {
      initalizeAdLanguages()
    }, (error) => {
      Alert.alert('Language', 'Fail to set languages, error: ' + error)
    })
  }
  //#endregion

  //#region I N T E R A C T I O N S
  function onTapActivateAdService() {
    const title = "Ad Service"
    const message = "Do you wish to activate your ad service?"
    Alert.alert(title, message,[
      {
        text: "Deactivate",
        onPress: () => activateService(false)
      },
      {
        text: "Activate",
        onPress: () => activateService(true)
      }
    ],{ cancelable: true, onDismiss: () => {} })
  }

  function onTapGetCategories() {
    getCategories()
  }
  //#endregion

  //#region H E L P E R S
  function languagesNamesStringFromArray(languages) {
    return languages.map((language) => {
      return language.name
    }).join(', ')
  }

  function printCategories(categories) {
    for (var i = 0 ; i < categories.length ; i++) {
      const category = categories[i]
      console.log(`=> Category: ${category.name}`)
      for (var j = 0 ; j < category.subcategories.length ; j++) {
        const subcategory = category.subcategories[j]
        console.log(`=> == [${subcategory.isUserInterested}] SubCat: (${subcategory.id}) ${subcategory.name}`)
      }
    }
  }
  //#endregion
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  mobadStatusContainer: {
    marginTop: 20,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  mobadHighlight: {
    fontWeight: 'bold',
  },
});

export default App;
