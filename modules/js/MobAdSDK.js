import { NativeModules } from 'react-native';
const { MobAdiOS: mobAdiOS } = NativeModules;
const { MobAdAndroid: mobAdAndroid } = NativeModules;
import { Platform } from 'react-native';

export default class MobAdSDK {
	//The android specific SDK functions instance
	android = new MobAdAndroidSpecificSDK()
	ios = new MobAdiOSSpecificSDK()
	
	constructor() {}

	/**
	 * Returns whether the ad service is enabled or not
	 * @returns {boolean} Whether the service is enabled or not
	 */
	isAdServiceEnabled() {
		if (PlatformHelpers.isAndroid()) {
			return mobAdAndroid.getAdsEnabled()
		} else {
			return mobAdiOS.isAdServiceActive()
		}
	}

	/**
	 * Activates or deactivate the ad service for the user
	 * 
	 * @async
	 * @param {boolean} activate The desired state of the ad service
	 * @returns {Promise<null>} Returns null if sucess and Error if error
	 */
	async activateAdService(activate) {
		if (PlatformHelpers.isAndroid()) {
			return mobAdAndroid.setAdsEnabled(activate)
		} else {
			return mobAdiOS.activateAdService(activate)
		}
	}

	/**
	 * Returns whether the initialize user was previously called with success or not
	 * @returns {boolean} Whether the user is signed in or not
	 */
	isSignedIn() {
		if (PlatformHelpers.isAndroid()) {
			return mobAdAndroid.isUserInitialized()
		} else {
			return mobAdiOS.isSignedIn()
		}
	}

	/**
	 * Returns the maximum number of ads that user desires to see per day
	 * @returns {number} The max number of ads per day for the user
	 */
	getMaximumAdsPerDay() {
		if (PlatformHelpers.isAndroid()) {
			return mobAdAndroid.getMaximumAdsPerDay()
		} else {
			return mobAdiOS.getUserMaximumAdsPerDay()
		}
	}

	/**
	 * Sets the max number of ads that the user wishes to receive per day
	 * 
	 * @async
	 * @param {number} maxAdsPerDay The desired max number of ads
	 * @returns {Promise<number>} Returns the new number of ads per day if success (if the desired number exceeds the limit, the limit is returned here) and Error if error
	 */
	async setMaximumAdsPerDay(maxAdsPerDay) {
		if (PlatformHelpers.isAndroid()) {
			try {
				const result = await mobAdAndroid.setMaximumAdsPerDay(maxAdsPerDay)
				return result.newMaxAdsPerDayValue
			} catch (e) {
				throw e
			}
		} else {
			return mobAdiOS.updateUserProfileWithCountryCode(null, null, maxAdsPerDay, null)
		}
	}

	/**
	 * This function should be called on every app open to refresh the ad service
	 * 
	 * @async
	 * @returns {Promise<null>} Returns null on success
	 */
	async activate() {
		if (PlatformHelpers.isAndroid()) {
			mobAdAndroid.startMobAdService(false, null)
		} else {
			mobAdiOS.activate(null)
		}
	}

	/**
	 * This function should be the first encounter between the user device and mobAd SDK. It should be called once on the first app launch. It registers the user/device on our database
	 * 
	 * @async
	 * @param {string} email The email of the user. Null if the app takes no emails by default
	 * @param {string} password The password of the user (in case there was an email, null otherwise)
	 * @param {string} languageCode The language of the app (if the app is single language). If left null, the language of the phone will be used
	 * @param {string} countryCode The country code of the user. If null, it is autodetected from the device. (works on iOS only, android will detect the device's code in all cases) 
	 * @returns {Promise<null>} Returns null on success, Error on error
	 */
	async initializeUser(email = null, password = null, languageCode = null, countryCode = null) {
		if (PlatformHelpers.isAndroid()) {
			return mobAdAndroid.initializeUser(email, password, languageCode)
		} else {
			return mobAdiOS.initiateUserWithEmail(email, password, countryCode, languageCode);
		}
	}

	/**
	 * This function returns all the categories and subcategories of the SDK at the language of the user. Also the subctegory object has a key "isUserInterested" that is true if the user selected this subcategory
	 * 
	 * @async
	 * @returns {Promise<Category[]>} A list of Category object
	 */
	async getCategories() {
		var categories = []
	
		if (PlatformHelpers.isAndroid()) {
			try {
				const categoriesJSArray = await mobAdAndroid.getCategories()
				categories = categoryInstancesFromAndroidResult(categoriesJSArray)
			} catch (e) {
				throw e
			}
		} else {
			try {
				const listOfInterests = await mobAdiOS.allInterests()
				const listOfUserInterests = await mobAdiOS.getUserInterests()
				categories = categoryInstancesFromIOSResult(listOfInterests, listOfUserInterests)
			} catch (e) {
				throw e
			}
		}

		return categories
	}

	/**
	 * This function sets the subcategories from which the user wishes to receive ads
	 * 
	 * @async
	 * @param {string[]} subcategoryIds An array containing the codes of the subcategories that the user wishes to have as their preferences
	 * @returns {Promise<null>} Returns null if success and Error if error
	 */
	async updateInterests(subcategoryIds) {
		if (PlatformHelpers.isAndroid()) {
			return mobAdAndroid.updateInterests(subcategoryIds.map((item) => {
				const subcategory = new Subcategory()
				subcategory.id = item
				subcategory.isUserInterested = true
				return subcategory
			}))
		} else {
			return mobAdiOS.setUserInterestsWithSubcategoriesIds(subcategoryIds)
		}
	}

	/**
	 * This function returns all the supported languages of the SDK. Also it contains a key is Selected that is true if the user selected this language
	 * 
	 * @async
	 * @returns {Promise<Language[]>} A list of Language object
	 */
	async getLanguages() {
		var languages = []
		if (PlatformHelpers.isAndroid()) {
			try {
				const languagesJSArray = await mobAdAndroid.getAdLanguages()
				languages = languageInstancesFromAndroidResult(languagesJSArray)
			} catch (e) {
				throw e
			}
		} else {
			try {
				const listSupportedLanguages = await mobAdiOS.getSupportedLanguages()
				const listOfUserPreferredLanguages = mobAdiOS.getPreferredAdLanguages()
				languages = languageInstancesFromiOSResult(listSupportedLanguages, listOfUserPreferredLanguages)
			} catch(e) {
				throw e
			}
			
		}
		return languages
	}

	/**
	 * This function sets languages at which the user prefers to receive ads
	 * 
	 * @async
	 * @param {int[]} languageIds An array containing the IDs of the languages at which the user wishes to receive ads
	 * @returns {Promise<null>} Returns null if success and Error if error
	 */
	async updateAdLanguages(languageCodes) {
		if (PlatformHelpers.isAndroid()) {
			return mobAdAndroid.updateAdLanguages(languageCodes.map((item) => {
				return {
					id: 0,
					code: item,
					name: "",
					native_name: "",
					is_selected: true
				}
			}))
		} else {
			return mobAdiOS.updateUserProfileWithCountryCode(null, null, -1, languageCodes)
		}
	}

	/**
	 * This function should be called to request an intertitial overlay ad (usually on app open)
	 * 
	 * @async
	 * @returns {Promise<null>} Returns null on success
	 */
	async requestInterstitialAd(delayMillis = 0) {
		if (PlatformHelpers.isAndroid()) {
			mobAdAndroid.requestInterstitialAd(delayMillis)
		} else {
			const delaySeconds = delayMillis/1000
			mobAdiOS.requestInAppInvasiveAd(delaySeconds)
		}
	}

	/**
	 * Returns the email of the user if they are signed in
	 * @returns {boolean} Returns the email of the user if they are signed in using email and null otherwise or if the user is not signed in
	 */
	getUserEmail() {
		if (PlatformHelpers.isAndroid()) {
			return mobAdAndroid.getUserEmail()
		} else {
			return this.ios.getUser().email
		}
	}

	/**
	 * Checks if the firebase notification object sent as parameter is a MobAd notification
	 *
	 * @param {Omit<ReceivedNotification, "userInfo">} notification The notification object retrieved in the Firebase onNotification
	 * @returns {boolean} Returns true if the notification is from MobAd
	 */
	firebaseIsMobAdNotification(notification) {
		return notification.data.mobad_notification_type != undefined
	}
}

//#region Android Specific
class MobAdAndroidSpecificSDK {
	/**
	 * Returns whether the user enabled the notification listener settings
	 * @returns {boolean} Returns true if the user enabled the notification listener settings
	 */
	checkUserEnabledNotificationListenerSettings() {
		return mobAdAndroid.checkUserEnabledNotificationListenerSettings()
	}
	
	/**
	 * Takes the user to the settings page where they can enable the notification listener settings for your application
	 * @async
	 * @returns {Promise<null>} Returns null on success
	 */
	async goToNotificationListenerSettings() {
		mobAdAndroid.goToNotificationListenerSettings()
	}

	/**
	 * Promts the user to give all the mobAd permissions and starts the mobAd service after all the permissions are granted
	 * @async
	 * @returns {Promise<null>} Returns null on success
	 */
	async checkMobAdPermissionsAndStartService() {
		mobAdAndroid.checkMobAdPermissionsAndStartService(null)
	}

	/**
	 * Returns whether the user enabled the read phone state permission
	 * @returns {boolean} Returns true if the user enabled the read phone state permission
	 */
	hasReadPhoneStatePermission() {
		return mobAdAndroid.hasReadPhoneStatePermission()
	}

	/**
	 * This function displays the popup that will prompt the user to grant the read phone state permission
	 * 
	 * @async
	 * @returns {Promise<boolean>} Returns (after the user response) true if the permission was granted (or if it was already granted) and false otherwise
	 */
	async requestReadPhoneStatePermission() {
		return mobAdAndroid.requestReadPhoneStatePermission()
	}

	/**
	 * Takes the user to the settings page where they can enable the draw over other apps permission
	 * @async
	 * @returns {Promise<null>} Returns null on success
	 */
	async requestDrawOverAppsPermission() {
		mobAdAndroid.requestDrawOverAppsPermission()
	}

	/**
	 * Returns whether the user enabled the draw over other apps permission
	 * @returns {boolean} Returns true if the user enabled the draw over other apps permission
	 */
	canDrawOverOtherApps() {
		return mobAdAndroid.canDrawOverOtherApps()
	}

	/**
	 * Returns whether the user tried to initialize lately but failed (in case you might need to reinitialize)
	 * @returns {boolean} Returns true if the user has failed the init user process
	 */
	isUserFailedInit() {
		return mobAdAndroid.isUserFailedInit()
	}

	/**
	 * Cancels the init user process if it is still in progress
	 * @async
	 * @returns {Promise<null>} Returns null on success
	 */
	async cancelInitUser() {
		mobAdAndroid.cancelInitUser()
	}
}
//#endregion

//#region iOS Specific
class MobAdiOSSpecificSDK {
	/**
	 * Returns the primary language code amongst the user's ad languages
	 * @returns {string} Returns the a two letter string referring to the language code based on 'ISO 639-1'
	 */
	getLanguageCode() {
		return mobAdiOS.getLanguageCode()
	}

	/**
	 * Returns the signed in user object
	 * @returns {User} A user object
	 */
	getUser() {
		return userInstanceFromiOSResult(mobAdiOS.getUser())
	}

	/**
	 * Retrieve the signed in user object from server and return it
	 * @async
	 * @returns {Promise<User>} A user object
	 */
	async getUserProfile() {
		return mobAdiOS.getUserProfile()
	}

	/**
	 * Required call to be called at least once before any usage on the sdk.
	 * It initializes the sdk with the required `App Identifier` and `App Group`.
	 * The `App Group` should be shared between the app and the content notification extension.
	 * @returns {Promise<User>} A user object
	 */
	initialize(identifier, group) {
		mobAdiOS.initializeWithIdentifier(identifier, group);
	}

	/**
	 * Registered a user to APNs with the given token
	 * @async
	 * @param {string} token A token generated by the device on each launch 
	 * @returns {Promise<null>} A promise with a null value on success
	 */
	async registerRemoteNotification(token) {
		return mobAdiOS.registerRemoteNotificationsWithToken(token)
	}

	/**
	 * Schedule a stubbed advertisement for testing puposes
	 * @param after A number in milliseconds referring to delay before showing the notification
	 * @param type the ad type [0:carousel ; 1:image ; 2:text ; 3:video]
	 * @returns {undefined} 
	 */
	scheduleAdNotification(after = 2000, type = 0) {
		mobAdiOS.scheduleAdNotificationAfter(after, type)
	}

	/**
	 * Sign a user out
	 * @returns {undefined} 
	 */
	signOutUser() {
		mobAdiOS.signOutUser()
	}

	/**
	 * Retrieve the ads to be displayed today if needed, and cach available media for faster ad loading.
	 * @returns {undefined} 
	 */
	async syncAds() {
		return mobAdiOS.syncAdsInBackground()
	}

	/**
	 * Retrieve the ads to be displayed today if needed, and cach available media for faster ad loading.
	 * @async
	 * @param {string} countryCode The 2 character string referring to the country code based on ISO 3166
	 * @returns {undefined} 
	 */
	async updateCountry(countryCode) {
		return mobAdiOS.updateUserProfileWithCountryCode(countryCode, null, -1, null)
	}
}
////#endregion

//#region Configuration
//Loop over all the android specific functions to make sure they won't run unless the platform is android
//Else they should return WrongPlatformError depending on if it's async or not
for(const key of Object.getOwnPropertyNames(MobAdAndroidSpecificSDK.prototype)) {
	const currentFunction = MobAdAndroidSpecificSDK.prototype[key];
	MobAdAndroidSpecificSDK.prototype[key] = function(...args) {
	  if (PlatformHelpers.isAndroid()) {
		return currentFunction.call(this, ...args);
	  } else {
		//Check if the function is async or not  
		if (isFuncAsync(currentFunction)) {
			//Create an async function that returns the wrong platform error
 			return (async function() {
				return new WrongPlatformError()
			}).call() 
		} else {
			return new WrongPlatformError()
		}
	  }
	};
}

//Loop over all the iOS specific functions to make sure they won't run unless the platform is ios
//Else they should return WrongPlatformError depending on if it's async or not
for(const key of Object.getOwnPropertyNames(MobAdiOSSpecificSDK.prototype)) {
	const currentFunction = MobAdiOSSpecificSDK.prototype[key];
	MobAdiOSSpecificSDK.prototype[key] = function(...args) {
	  if (PlatformHelpers.isIos()) {
		return currentFunction.call(this, ...args);
	  } else {
		//Check if the function is async or not  
		if (isFuncAsync(currentFunction)) {
			//Create an async function that returns the wrong platform error
 			return (async function() {
				return new WrongPlatformError()
			}).call() 
		} else {
			return new WrongPlatformError()
		}
	  }
	};
}
//#endregion



//#region Helpers
class PlatformHelpers {
	static isAndroid() {
		return Platform.OS == "android"
	}

	static isIos() {
		return Platform.OS == "ios"
	}
}

function isFuncAsync(func) {
	let functionContents = func.toString();
	let functionBody = functionContents.slice(functionContents.indexOf("{") + 1, functionContents.lastIndexOf("}"))
	return functionBody.trim().startsWith("return _regenerator.default.async")
}
//#endregion

//#region Custom
class WrongPlatformError extends Error {
	constructor() {
		super()
		this.name = "WrongPlatformError"
	}
}
//#endregion

//#region Models
class Category {
	id
	name
	subcategories
}

class Subcategory {
	id
	name
	isUserInterested
}

class Language {
	code
	name
	nativeName
	isSelected
}

class User {
	countryCode
	email
}

//#endregion

//#region Model Helers
function categoryInstancesFromAndroidResult(androidResult) {
	return androidResult.map(item => {
		const category = new Category()
		category.id = item.id
		category.name = item.name
		category.subcategories = item.subcategories.map((subItem) => {
			const subcategory = new Subcategory()
			subcategory.id = subItem.id
			subcategory.name = subItem.name
			subcategory.isUserInterested = subItem.isUserInterested
			return subcategory
		})
		return category
	})
}

function categoryInstancesFromIOSResult(iOSListOfInterests, iOSListOfUserInterests) {
	return iOSListOfInterests.allCategories.map((item) => {
		const category = new Category()
		category.id = item.id
		category.name = item.name
		category.subcategories = iOSListOfInterests.allSubcategories.filter((subItem) => {
			return subItem.categoryId == item.id
		}).map((subItem) => {
			const subcategory = new Subcategory()
			subcategory.id = subItem.id
			subcategory.name = subItem.name
			subcategory.isUserInterested = iOSListOfUserInterests.indexOf(subItem.id) > -1
			return subcategory
		})
		return category
	})
}

function languageInstancesFromAndroidResult(androidResult) {
	return androidResult.map((item) => {
		const language = new Language()
		language.code = item.code
		language.name = item.name
		language.nativeName = item.native_name
		language.isSelected = item.is_selected
		return language
	})
}

function languageInstancesFromiOSResult(supportedLanguages, userPreferredLanguages) {
	return supportedLanguages.map((item) => {
		const language = new Language()
		language.code = item.code
		language.name = item.name
		language.nativeName = item.nativeName
		language.isSelected = userPreferredLanguages.indexOf(item.code) > -1
		return language
	})
}

function userInstanceFromiOSResult(iOSResult) {
	const user = new User()
	user.countryCode = iOSResult.countryCode
	user.email = iOSResult.email
	return user
}
//#endregion
