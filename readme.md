# MobAd SDK React Native

The following instructions will allow you to integrate the MobAd SDK in React Native app using Android Studio, Xcode and any IDE for React Native.
- [Installation](#installation)
- [Usage](#usage)

## Installation

This section is divided by platform:
- [Android Installation](#android-installation)
- [iOS Installation](#ios-installation)
- [React Native Installation](#react-native-installation)

### Android Installation

The installation of the SDK for Android is divided into two main sections:
- [Android Configuration ](#android-configuration): Make ModAd work
- [Android Module Integration](#android-module-integration): Export the SDK methods to JS

#### Android configuration

The steps below will guide you through the steps required to configure the MobAd SDK on the Android side. In order to do these steps, you will have to open Android Studio and set your project location to the android folder of your react native project.

**Step 1**. In your gradle.properties file define a variable called `authToken` with the authToken that we have provided. This variable is essential for being able to access the private jipack repository

```
authToken=<your_auth_token>
```

**Step 2**. Add the JitPack repository to your build file
Add it in your root build.gradle at the end of repositories.
Notice the credentials attribute that contains the `authToken` parameter you defined earlier.

```
allprojects {
    repositories {
        ...
        maven { 
            url 'https://jitpack.io'
            credentials { username authToken }
        }
    }
}
```

**Step 3**. Add the Huawei HMS SDK repository right under the jitpack repository
Note: Make sure to add Huawei's HMS maven in all your repositories section.

```
allprojects {
    repositories {
        ...
        maven { 
            url 'https://jitpack.io'
            credentials { username authToken }
        }
        maven {url 'https://developer.huawei.com/repo/'
    }
}
```
**Step 4**. Add the Huawei HMS classpath inside the dependencies of the buildscript the build.gradle of the project.

```
buildscript {
    ...
    dependencies {
        ...
         classpath 'com.huawei.agconnect:agcp:y'
    }
}
```
Note: Let y be the latest version of this classpath

**Step 5**. In the app gradle file, add the line that sets the provided App ID to enable our server to identify and authenticate your app.

```
android {
    ...
    defaultConfig {
        ...
        manifestPlaceholders = [app_id: "<your_app_id>"]
    }
}
```
**Step 6**. Add the dependency for the SDK in the app level gradle file

```
dependencies {
    implementation 'com.gitlab.imw-developers:mobad-android-sdk:x.x.x'
}
```

(You have to replace the `x.x.x` with the latest version of the SDK that you can find [here](https://mobad.azurewebsites.net/sdk/latest_gradle_version))

Note:
>If you want to test it's preferable to do it with our dedicated for-testing servers, you'll have to add -Testing to your version (Example: 1.1.2-Testing / 1.1.2h-Testing).

**Step 7**. Add `multidex` as a dependency and enable `multidex`for the project (if not present before).

```
android {
    ...
    defaultConfig {
        ...
        multiDexEnabled true
    }
}
...
dependencies {
    //Multidex library
    def multidex_version = "2.0.1"
    implementation "androidx.multidex:multidex:$multidex_version"
}
```

**Step 8.** Add the dependency for the GSON library in the app level gradle file to help in JSON manipulation between Java and JS

```
dependencies {
	//GSON for JSON handling
    implementation 'com.google.code.gson:gson:2.8.6'
}
```

**Step 9**. This step is optional if there was a conflict when building the project between microsoft azure sdk and your app regarding the `android icon` and the `allowBackup` functionality. You can solve it by adding to your `manifest` tag in the `AndroidManifest.xml` file

> `xmlns:tools="http://schemas.android.com/tools".` 

Then add to the application tag
>`tools:replace="android:allowBackup,android:icon"` 

The whole thing would resemble the following
```
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools">

    <application
        ...
        tools:replace="android:icon">
    ...
    </application>
</manifest>
```



#### Android Module Integration

The next steps will guide you into creating a wrapper module for the MobAd Android SDK using Java to be called in Javascript code. These steps will be done in the `android` folder using Android Studio as well.

**Step 1.** Create a Java class file in your android directory (wherever you find it best) and name it **MobAdModule**. Then copy the content of the file `modules/android/MobAdModule.java` inside.

**Step 2.** Create a java class file in your android directory (wherever you find it best) name it **MobAdPackage**. Then copy the content of the file `modules/android/MobAdPackage.java` inside.

**Step 3.** Add the instance of the package to the list of react packages in the custom application class (by default: **MainApplication.java**):

```java
// Packages that cannot be autolinked yet can be added manually here, for example:
// packages.add(new MyReactNativePackage());
...
packages.add(new MobAdPackage());
...
return packages;
```

---

### iOS Installation

The installation of the SDK for iOS is divided into two main sections:
- [iOS Configuration ](#ios-configuration): Make ModAd work
- [iOS Module Integration](#ios-module-integration): Export the SDK methods to JS

#### iOS Configuration

The steps below will guide you through the steps required to configure the MobAd SDK on the iOS side.

**Step 1.**  Create a new `Notification Content Extension` for your app

**Step 2.** Make sure that the `Notification Content Extension` target version is similar to the main app target version

**Step 3.** Add for both the `application` & the `notification content extension` the **`App Group`** capability 

**Step 4.** Add MobAdSDK in podfile
- Check for the latest version by searching the pods repository and replace the `x.x.x`
- Add the library for the app & the newly created `Notification Content Extension`
- Add a post script for allowing the use of application methods in the extension

```ruby
require_relative '../node_modules/react-native/scripts/react_native_pods'
require_relative '../node_modules/@react-native-community/cli-platform-ios/native_modules'

platform :ios, '10.0'

target 'MobadSampleApp' do
  config = use_native_modules!

  use_react_native!(:path => config["reactNativePath"])
  
  pod 'MobAdSDKObjc', 'x.x.x'

  target 'MobadSampleAppTests' do
    inherit! :complete
    # Pods for testing
  end

  # Enables Flipper.
  #
  # Note that if you have use_frameworks! enabled, Flipper will not work and
  # you should disable these next few lines.
#  use_flipper!({ 'Flipper' => '0.76.0' })
  post_install do |installer|
    flipper_post_install(installer)

    #Specify what and where has to be added
    targetName = 'MobAdSDKObjc'

    # Key & Value to allow the use of UIApplication shared instance
    keyApplicationExtensionAPIOnly = 'APPLICATION_EXTENSION_API_ONLY'
    valueApplicationExtensionAPIOnly = 'NO'

    #Find the pod which should be affected
    targets = installer.pods_project.targets.select { |target| target.name == targetName }
    target = targets[0]

    #Do the job
    target.build_configurations.each do |config|
        config.build_settings[keyApplicationExtensionAPIOnly] = valueApplicationExtensionAPIOnly
    end
  end
end

target 'AdNotificationExtension' do
  pod 'MobAdSDKObjc', 'x.x.x'
end
```

**Step 5.** Open the `info.plist` file of the newly created extension as source code and change the value of the following attributes to:

```
<key>UNNotificationExtensionCategory</key>
<string>MobAdSDKNotificationCategoryGeneric</string>
<key>UNNotificationExtensionDefaultContentHidden</key>
<true/>
<key>UNNotificationExtensionInitialContentSizeRatio</key>
<real>0.1</real>
```

**Step 6.** Open the view controller in the newly created target and set the class `MobAdNotificationViewController` as its superclass

```swift
import MobAdSDK

class NotificationViewController: MobAdNotificationViewController { }
```

**Step 7.** Now open the generated iOS project and go to the **AppDelegate.m** file.
When the application finishes launching, initialize the SDK with the `APP_ID` generated for the app by the **MobAd team**, along with the `APP_GROUP_STRING`.

```swift
#import <MobAdSDK/MobAdSDK-Swift.h>

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
...
  [MobAdSDK.shared initializeWithIdentifier:@"APP_ID" group:@"APP_GROUP_STRING"];
...
  return true
}
```

**Step 8.** Handle ad notification interaction in the AppDelegate by calling the SDK handler in the related `UserNotification` framework method

1. Set the `UserNotificationCenter` delegate
```swift
#import <UserNotifications/UserNotifications.h>
#import <MobAdSDK/MobAdSDK-Swift.h>

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
...
  // 1. Set the `UserNotificationCenter` delegate
  [UNUserNotificationCenter.currentNotificationCenter setDelegate:self];

  [MobAdSDK.shared initializeWithIdentifier:@"APP_ID" group:@"APP_GROUP_STRING"];
...
  return true
}
```
2. Implement method for catching these calls
```swift
// 2. Implement method for catching these calls
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
  if ([MobAdSDK.shared handle:response]) {
    return;
  } else {
    // Handle Notification
  }
}
```

**Step 9.** Support displaying the ad notification when application is in the foreground . 
If the notification is handled by the SDK **do not** call the `completionHandler`.

```
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
  if ([MobAdSDK.shared handleForegroundNotification:notification completion:completionHandler]) {
    return;
  } else {
    // Handle Notification
    completionHandler(UNNotificationPresentationOptionNone);
  }
}
```



##### Synchronizing Ads

Implementing this features would allow the sdk to schedule ads based on their CAP throughout the day.
Scheduled ads will have their media downloaded in the background when given the chance, to make sure that users with low internet speed can have the best experience when an ad is displayed.
The add scheduling and caching is triggered in the background in two different ways:
- Silent Remote Notifications
- Background App Refresh

**Step 10.** Register for remote notifications when application finishes launching

1. Add remote notification capability

2. Request notification authorization
```swift
  [UNUserNotificationCenter.currentNotificationCenter requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert) completionHandler:^(BOOL granted, NSError * _Nullable error) { }];
  
  [UNUserNotificationCenter.currentNotificationCenter setDelegate:self];
```

3. Register app for APNs remote notification
```swift
[UNUserNotificationCenter.currentNotificationCenter setDelegate:self];

[UNUserNotificationCenter.currentNotificationCenter getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
  if (settings.authorizationStatus == UNAuthorizationStatusAuthorized) {
    dispatch_async(dispatch_get_main_queue(), ^{
      [[UIApplication sharedApplication] registerForRemoteNotifications];
    });
  }
}];
```

4. Register to MobAd silent notifications
```swift
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
  NSString * deviceTokenString = [[[[deviceToken description]
                       stringByReplacingOccurrencesOfString: @"<" withString: @""]
                      stringByReplacingOccurrencesOfString: @">" withString: @""]
                     stringByReplacingOccurrencesOfString: @" " withString: @""];
  
  [MobAdSDK.shared registerRemoteNotificationsWithToken:deviceTokenString completion:^(BOOL success) {}];
}
```

**Step 11.** Implement _Background App Refresh_
([guide](https://developer.apple.com/documentation/uikit/app_and_environment/scenes/preparing_your_ui_to_run_in_the_background/updating_your_app_with_background_app_refresh))

1. Enable the background fetch capability in your app

2. Call the  [`setMinimumBackgroundFetchInterval(_:)`](https://developer.apple.com/documentation/uikit/uiapplication/1623100-setminimumbackgroundfetchinterva)  method of  [`UIApplication`](https://developer.apple.com/documentation/uikit/uiapplication)  at launch time.
```swift
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  ...
  NSTimeInterval twoHoursInSeconds = 60*60*2;
  [application setMinimumBackgroundFetchInterval:twoHoursInSeconds];
  ...
}
```

3. Implement the  [`application(_:performFetchWithCompletionHandler:)`](https://developer.apple.com/documentation/uikit/uiapplicationdelegate/1623125-application)  method in your app delegate
```swift
- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
  [MobAdSDK.shared syncAds:RequestTriggerBackgroundFetch completion:^(UIBackgroundFetchResult result) {
    completionHandler(result);
  }];
}
```

4. Handle Background Session If app was terminated while performing the background app refresh ([guide](https://www.raywenderlich.com/3244963-urlsession-tutorial-getting-started#toc-anchor-017))
> Make sure to send the completion to the sdk so it would be able to resume the media saving process after the completion of a download 
```swift
- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)(void))completionHandler {
  MobAdSDK.shared.backgroundSessionCompletionHandler = completionHandler;
}
```

#### iOS Module Integration

The next steps will guide you into exposing the MobAdSDK methods to JavaScript

**Step 1.** Create an Objective-C Header file (.h) and name it **RCTMobAdiOS**. Then replace the content with the following code (or with the content of the file at  `modules/ios/RCTMobAdiOS.h`)

```swift
#import <React/RCTBridgeModule.h>

@interface MobAdiOS : NSObject<RCTBridgeModule>

@end
```

**Step 2.** Create an Objective-C file (.m) and name it **RCTMobAdiOS**. Then copy the content of the file  `modules/ios/RCTMobAdiOS.m` inside.

---

### React Native Installation

In order to complete the installation of the library you will have to create a Javascript file that contains a wrapper for the Android and iOS native libraries. 

You can use the wrapper present in this repository called **MobAdSDK.js**. You can find it here:
> `modules/js/MobAdSDK.js`

This concludes the installation process on the all of the Android, iOS and React Native sides.

---


## Usage

In order to use MobAd in your JavaScript code you have to first:

1. Import the MobAd class
```js
import MobAd from './MobAdSDK';
```

2. Instantiate an instance of MobAd to be able to call any SDK method
```js
var mobAd = new MobAd();
```

3. Initialize a user
Initializing a user should be called only once. 
Generally speaking it's when he is notified that he will be receiving ads from now on. 
> Required before calling any other functionalities
> All parameters can have a `null` value 
```js
mobAd.initializeUser(email, password, languageCode, countryCode).then(() => {
  console.log('User initialized successfully')
}, (error) => {
  console.log(`Something went wrong - error: ${error}`)
})
```

4. Call the `activate()` method every time the application launches
```js
mobAd.activate()
```

There are other methods that are useful to retrieve and change a user's preference.
These preferences are:

- Activate/Deactivate service
```js
mobAd.isAdServiceEnabled()
mobAd.activateAdService(activate) //async
```

- User interests
```js
mobAd.getCategories() //async
mobAd.updateInterests(subcategoryIds) //async
```

- Preferred ad languages
```js
mobAd.getLanguages() //async
mobAd.updateAdLanguages(languageCodes) //async
```

- Maximum ads per day
```js
mobAd.getMaximumAdsPerDay()
mobAd.setMaximumAdsPerDay(maxAds) //async
```

Other methods that come handy

```js
mobAd.isSignedIn()
mobAd.getUserEmail()
```

### Request Interstitial Ad 
In order to request an interstitial in-app advertisement 
> Recommended on app open
```js
// This method can take milliseconds for delaying the display of the ad
mobAd.requestInterstitialAd() //async
```

### Handling MobAd push notifications

If your app sends push notifications, there is a chance that it might conflict with push notifications sent by MobAd. In order to avoid the conflict, a set of steps must be done.

#### Firebase Notifications

If your app uses firebase, you will have to send the notification object retrieved from the firebase handler `onNotification` to a function that we created that analyses the notification and checks if this a MobAd notification or not. In case it is a MobAd notification, you must stop handling the notification and discard it.

In order to check if the firebase notification is from MobAd:

```js
mobAd.firebaseIsMobAdNotification(firebaseNotification) //Callback to be disregarded if this function returns true
```

#### APNs Notifications

Check the [iOS Configuration](#ios-configuration) section


### Android Specific functionalities

One difference between the android and iOS SDKs for MobAd is that on Android, the user has to give a set of permissions explicitly while using the app to be able to use the service at its fullest. The permissions are the following:

- **READ_PHONE_STATE** permission to be able to display ads at the end of phone calls
- **Draw over other apps** permission to be able to show the ad bubble
- **Notifications listener** permission to be able to display ads at the end of VoIP calls

In order to prompt the user to allow these permissions, MobAd SDK contains a set of helper functions that make requesting the permissions easier on the integrating apps' developers.

#### Method 1:  Allowing the permissions one by one (recommended)

In order to get the user to allow the permissions while knowing why we request them, we recommend creating a screen for each permission, that explains why we request it and that contains two buttons, one that prompts to user to allow the permission, and another one that the user might click if they don't want to allow it and wish to proceed in all cases. We recommend putting an emphasis on the first button and to deemphasize the second one.

##### Read phone state permission

In order to prompt the user to allow the read phone state permission:

```js
mobAd.android.requestReadPhoneStatePermission().then(granted => {
    if (granted) {
        //Todo when permission is granted by the user after the prompted or if it was previously granted
    } else {
        //Todo when permission is revoked by the user
    }
})
```

In order to check if the user enabled the read phone state permission:

```js
mobAd.android.hasReadPhoneStatePermission() //Returns true if the user has granted the permission and false otherwise
```

##### Draw over other apps permission

In order to take the user to the settings to allow the draw over other apps permission:

```js
mobAd.android.requestDrawOverAppsPermission()
```

In order to check if the user enabled the draw over other apps permission:

```js
mobAd.android.canDrawOverOtherApps() //Returns true if the user has granted the permission and false otherwise
```

##### Notifications listener permission

In order to take the user to the settings to allow the notification listener permission:

```js
mobAd.android.goToNotificationListenerSettings()
```

In order to check if the user already enabled the notification listener permission:

```js
mobAd.android.checkUserEnabledNotificationListenerSettings() //Returns true if the permission was given and false otherwise
```

#### Method 2: Request all the permissions together and activate the service (not recommended)

Alternatively, you can replace the **activate** call mentioned in the common section above, with a call that:

* Keeps requesting the non granted permissions each time the function is called
* Activates the MobAd service when all the permissions are granted

This method is not recommended because it will request the permissions from the user each time the app is open without explaining the reason why we are requesting them, and will isolate the users from the MobAd service until they grant all the permissions.

In order to use it:

```js
mobAd.android.checkMobAdPermissionsAndStartService()
```

#### Additional Android functions

There are some additional functions that you can use on Android devices:

##### Cancel initialize user

In order to cancel the initialize user process in case it was still in progress:

```js
mobAd.android.cancelInitUser()
```

##### Check if user initialize failed

In order to check if the user tried to initialize lately but the request has failed (in this case you might need to re-initialize):

```js
mobAd.android.isUserFailedInit() //Returns true if the user tried to initialize before and failed
```



### iOS Specific functionalities

The iOS SDK holds some unique set of methods that can be used for multiple reason.
They are exposed to JS for you to make use of then if you need to.

- Initialize SDK
Required call to be called at least once before any usage on the sdk.
It initializes the sdk with the required `App Identifier` and `App Group`.
The `App Group` should be shared between the app and the content notification extension.

```js
mobAd.ios.initialize(identifier, group)
```

- Register to APNs 
Registered a user to APNs with the system's provided token

```js
mobAd.ios.registerRemoteNotification(token)
```

- Schedule a stubbed ad notification
Schedule a stubbed advertisement for testing puposes

```js
mobAd.ios.scheduleAdNotification() //async
```

- Get the user's default language

```js
mobAd.ios.getLanguageCode()
```

- Get user
Return the signed in user object

```js
// Retrieve the saved user profile
mobAd.ios.getUser()

// Retrieve the user profile from the server
mobAd.ios.getUser() // async
```

- Sign out user
Signs a user out. Which would require an `initializeUser` call for using the sdk functionalities again.

```js
mobAd.ios.signOutUser() // async
```

- Sync Ads
Retrieve the ads to be displayed today if needed, and cach available media for faster ad loading.

```js
mobAd.ios.syncAds() // async
```

- Update a user's country

```js
mobAd.ios.updateCountry() // async
```

---
---


That's all you need to do.
