//
//  RCTMobAd.m
//  ReactNativeWelcomeApp
//
//  Created by Marwan Toutounji on 3/16/21.
//

#import "RCTMobAdiOS.h"
#import <MobAdSDK/MobAdSDK-Swift.h>
#import <React/RCTLog.h>
#import <objc/runtime.h>

@implementation MobAdiOS

RCT_EXPORT_MODULE();

// MARK: SDK Variables
//====================
RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(isAdServiceActive) {
  return [NSNumber numberWithBool:[MobAdSDK.shared adServiceActive]];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(isSignedIn) {
  return [NSNumber numberWithBool:[MobAdSDK.shared isSignedIn]];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(getUser) {
  return [self dictionaryWithPropertiesOfObject:[MobAdSDK.shared user]];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(getUserMaximumAdsPerDay) {
  return [NSNumber numberWithLong:[MobAdSDK.shared userMaximumAdsPerDay]];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(getLanguageCode) {
  return [MobAdSDK.shared languageCode];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(getPreferredAdLanguages) {
  return [MobAdSDK.shared preferredAdLanguages];
}

// MARK: SDK - Methods
//====================
RCT_EXPORT_METHOD(initializeWithIdentifier:(NSString * _Nonnull)identifier group:(NSString * _Nullable)group) {
  [MobAdSDK.shared initializeWithIdentifier:identifier group:group];
}

RCT_EXPORT_METHOD(activate:(NSArray<NSNumber *> * _Nullable)events) {
  [MobAdSDK.shared activate:events];
}

RCT_EXPORT_METHOD(requestInAppInvasiveAd:(NSNumber * _Nonnull)delay) {
  [MobAdSDK.shared requestInAppInvasiveAdWithDelay:delay.doubleValue];
}

RCT_EXPORT_METHOD(scheduleAdNotificationAfter:(NSTimeInterval)interval
                  mediaType:(NSNumber * _Nonnull)mediaTypeValue) {
  MediaTypeCaste mediaType = [self mediaTypeFromNumber:mediaTypeValue];
  [MobAdSDK.shared scheduleAdNotificationAfter:interval mediaType:mediaType];
}

RCT_EXPORT_METHOD(signOutUser) {
  [MobAdSDK.shared signOutUser];
}

// MARK: SDK - API related methods
//================================
RCT_EXPORT_METHOD(initiateUserWithEmail:(NSString * _Nullable)email password:(NSString * _Nullable)password countryCode:(NSString * _Nullable)countryCode languageCode:(NSString * _Nullable)languageCode resolver:(RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject) {
  [MobAdSDK.shared initiateUserWithEmail:email password:password countryCode:countryCode languageCode:languageCode completion:^(BOOL success, enum MobAdError error) {
    if (success) {
      resolve(nil);
    } else {
      reject([NSString stringWithFormat:@"%ld", (long)error],
             [MobAdSDK mobAdErrorName:error],
             nil);
    }
  }];
}

RCT_EXPORT_METHOD(activateAdService:(BOOL)activate resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
  [MobAdSDK.shared adServiceWithActivate:activate completion:^(BOOL success, enum MobAdError error) {
    if (success) {
      resolve(nil);
    } else {
      reject([NSString stringWithFormat:@"%ld", (long)error],
             [MobAdSDK mobAdErrorName:error],
             nil);
    }
  }];
}

RCT_EXPORT_METHOD(allInterests:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
  [MobAdSDK.shared allInterestsWithCompletion:^(Interests * _Nullable interests, enum MobAdError error) {
    if (interests != NULL) {
      resolve([self interestsDictionary:interests]);
    } else {
      reject([NSString stringWithFormat:@"%ld", (long)error],
             [MobAdSDK mobAdErrorName:error],
             nil);
    }
  }];
}

RCT_EXPORT_METHOD(getSupportedLanguages:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
  [MobAdSDK.shared getSupportedLanguagesWithCompletion:^(BOOL success, NSArray<Language *> * _Nullable languages, enum MobAdError error) {
    if (success) {
      resolve([self languagesDictionaryArray:languages]);
    } else {
      reject([NSString stringWithFormat:@"%ld", (long)error],
             [MobAdSDK mobAdErrorName:error],
             nil);
    }
  }];
}

RCT_EXPORT_METHOD(getUserProfile:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
  [MobAdSDK.shared getUserProfileWithCompletion:^(BOOL success, User * _Nullable user, enum MobAdError error) {
    if (success) {
      resolve([self dictionaryWithPropertiesOfObject:user]);
    } else {
      reject([NSString stringWithFormat:@"%ld", (long)error],
             [MobAdSDK mobAdErrorName:error],
             nil);
    }
  }];
}

RCT_EXPORT_METHOD(getUserInterests:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
  [MobAdSDK.shared getUserInterestsWithCompletion:^(BOOL success, NSArray<NSNumber *> * _Nullable userInterests, enum MobAdError error) {
    if (success) {
      resolve(userInterests);
    } else {
      reject([NSString stringWithFormat:@"%ld", (long)error],
             [MobAdSDK mobAdErrorName:error],
             nil);
    }
  }];
}

RCT_EXPORT_METHOD(registerRemoteNotificationsWithToken:(NSString * _Nonnull) token resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
  [MobAdSDK.shared registerRemoteNotificationsWithToken:token completion:^(BOOL success) {
    if (success) {
      resolve(NULL);
    } else {
      reject(@"-1", @"", nil);
    }
  }];
}

RCT_EXPORT_METHOD(updateUserProfileWithCountryCode:(NSString * _Nullable)countryCode languageCode:(NSString * _Nullable)languageCode maxAdsPerDay:(NSNumber * _Nonnull)maxAdsPerDay preferredAdLanguagesCodes:(NSArray<NSString *> * _Nullable)preferredAdLanguagesCodes resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
  NSNumber * maxAds = NULL;
  if (maxAdsPerDay.intValue != -1) {
    maxAds = maxAdsPerDay;
  }
  
  [MobAdSDK.shared updateUserProfileWithCountryCode:countryCode languageCode:languageCode maxAdsPerDay:maxAdsPerDay preferredAdLanguagesCodes:preferredAdLanguagesCodes completion:^(BOOL success, NSNumber * _Nullable maxAds, enum MobAdError error) {
    if (success) {
      resolve(maxAdsPerDay);
    } else {
      reject([NSString stringWithFormat:@"%ld", (long)error],
             [MobAdSDK mobAdErrorName:error],
             nil);
    }
  }];
}

RCT_EXPORT_METHOD(setUserInterestsWithSubcategoriesIds:(NSArray<NSNumber *> * _Nonnull)subcategoriesIds resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
  [MobAdSDK.shared setUserInterestsWithSubcategoriesIds:subcategoriesIds completion:^(BOOL success, enum MobAdError error) {
    if (success) {
      resolve(NULL);
    } else {
      reject(@"-1", @"Failed to set user interests", nil);
    }
  }];
}

RCT_EXPORT_METHOD(syncAdsInBackground:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
  [MobAdSDK.shared syncAdsInBackgroundWithCompletion:^(UIBackgroundFetchResult fetchResult) {
    switch (fetchResult) {
      case UIBackgroundFetchResultFailed:
        reject(@"-1", @"Failed to fetch results in background", nil);
        break;
      case UIBackgroundFetchResultNoData:
        resolve([NSNumber numberWithBool:false]);
        break;
      case UIBackgroundFetchResultNewData:
        resolve([NSNumber numberWithBool:true]);
        break;
    }
  }];
}

// MARK: - HELPERS
//================================
- (NSDictionary *) interestsDictionary: (Interests *)interests {
  NSMutableDictionary * interestsDictionary = [NSMutableDictionary dictionary];
  
  NSArray * allCategories = interests.allCategories;
  NSMutableArray * allCategoriesArray = [NSMutableArray array];
  for (int i = 0; i < allCategories.count; i++) {
    NSDictionary * categoryDictionary = [self dictionaryWithPropertiesOfObject:allCategories[i]];
    [allCategoriesArray addObject:categoryDictionary];
  }
  [interestsDictionary setObject:allCategoriesArray forKey:@"allCategories"];
  
  NSArray * allSubcategories = interests.allSubcategories;
  NSMutableArray * allSubcategoriesArray = [NSMutableArray array];
  for (int i = 0; i < allSubcategories.count; i++) {
    NSDictionary * subcategoryDictionary = [self dictionaryWithPropertiesOfObject:allSubcategories[i]];
    [allSubcategoriesArray addObject:subcategoryDictionary];
  }
  [interestsDictionary setObject:allSubcategoriesArray forKey:@"allSubcategories"];
  
  return interestsDictionary;
}

- (NSArray *) languagesDictionaryArray: (NSArray<Language *> *)languages {
  NSMutableArray * languagesArray = [NSMutableArray array];
  for (int i = 0; i < languages.count; i++) {
    [languagesArray addObject:[self dictionaryWithPropertiesOfObject:languages[i]]];
  }
  return languagesArray;
}

- (NSDictionary *) dictionaryWithPropertiesOfObject:(id)obj
{
  if (obj == NULL) {
    return [NSDictionary dictionary];
  }
  
  NSMutableDictionary *dict = [NSMutableDictionary dictionary];
  
  unsigned count;
  objc_property_t *properties = class_copyPropertyList([obj class], &count);
  
  for (int i = 0; i < count; i++) {
    NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
    id value = [obj valueForKey:key];
    if (value != NULL) {
      [dict setObject:value forKey:key];
    }
  }
  
  free(properties);
  
  return [NSDictionary dictionaryWithDictionary:dict];
}

-(MediaTypeCaste) mediaTypeFromNumber: (NSNumber *) number {
  switch (number.intValue) {
    case 0:
      return MediaTypeCasteCarousel;
    case 1:
      return MediaTypeCasteImage;
    case 3:
      return MediaTypeCasteVideo;
    case 2:
    default:
      return MediaTypeCasteText;
  }
}

@end
